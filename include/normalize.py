import numpy as np

def normalizeStaining(I=None):
    Io=240
    beta=0.15
    alpha=1
    HERef = np.array([[0.5626,0.2159],[0.7201,0.8012],[0.4062,0.5581]])
    maxCRef = np.array([1.9705,1.0308])
    (h,w,c) = np.shape(I)
    I=np.reshape(I,(h*w,c),order='F')
    OD =- np.log((I + 1) / Io)
    ODhat=(OD[(np.logical_not((OD < beta).any(axis=1))),:])
    (W,V) = np.linalg.eig(np.cov(ODhat,rowvar=0))
    Vec =- np.transpose(np.array([V[:,1],V[:,0]])) #desnecessario o sinal negativo
    That=np.dot(ODhat,Vec)
    phi=np.arctan2(That[:,1],That[:,0])
    minPhi=np.percentile(phi,alpha)
    maxPhi=np.percentile(phi,100 - alpha)
    vMin = np.dot(Vec, np.array([np.cos(minPhi),np.sin(minPhi)]))
    vMax = np.dot(Vec, np.array([np.cos(maxPhi),np.sin(maxPhi)]))
    if vMin[0] > vMax[0]:
        HE=np.array([vMin,vMax])
    else:
        HE=np.array([vMax,vMin])
    HE = np.transpose(HE)
    Y = np.transpose(np.reshape(OD,(h*w,c)))
    C=np.linalg.lstsq(HE,Y,rcond=None)
    maxC=np.percentile(C[0],99,axis=1)
    C =  C[0]/maxC[:,None]
    C = C*maxCRef[:,None]
    Inorm = Io * np.exp(- np.dot(HERef,C))
    Inorm = np.reshape(np.transpose(Inorm),(h,w,c),order='F')
    Inorm = np.clip(Inorm,0,255);
    Inorm = np.array(Inorm, dtype=np.uint8)
    return Inorm

def normalizePictures(root_dir="./Test_data/", out_dir="./Test_data_normalized/"):
    for d_name, sd_list, f_list in os.walk(root_dir):
        if d_name == root_dir:
            continue
        labelname = os.path.basename(d_name)
        outputdir = os.path.join(out_dir,labelname)
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)
        for f in f_list:
            filepath = os.path.join(d_name,f)
            image = io.imread(filepath).astype("uint8").astype("float64")
            image = normalizeStaining(image)
            io.imsave(os.path.join(outputdir, f), image)
            
#normalizePictures()
#normalizePictures(root_dir="./Training_data/", out_dir="./Training_data_normalized/")
