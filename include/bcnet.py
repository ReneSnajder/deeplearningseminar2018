import torch.nn as nn

class BCnet(nn.Module):
    def __init__(self):
        super(BCnet, self).__init__()
        # indims 3x512x512
        
        self.relu = nn.ReLU(inplace=True)
        self.dropout = nn.modules.dropout.Dropout()
        
        # Edge detection
        self.l1_conv = nn.Conv2d(3,16,3) # outdims 16x510x510
        self.l2_pool = nn.MaxPool2d(3,3) # outdims 16x170x170
        
        # Nuclei
        self.l3_conv = nn.Conv2d(16,32,3) # outdims 32x168x168
        self.l4_pool = nn.MaxPool2d(2,2) # outdims 32x84x84
        self.l5_conv = nn.Conv2d(32,64,3, padding=1) # outdims 64x84x84
        self.l6_pool = nn.MaxPool2d(2,2) # outdims 64x42x42
        
        # Structure and tissue organization
        self.l7_conv = nn.Conv2d(64,64,3, padding=1) # outdims 64x42x42
        self.l8_pool = nn.MaxPool2d(3,3) # outdims 64x12x12
        self.l9_conv = nn.Conv2d(64,32,3) # outdims 32x12x12
        self.l10_pool = nn.MaxPool2d(3,3) # outdims 32x4x4
        
        # Classifier
        self.l11_fc = nn.Linear(512,256) # outdims 256
        self.l12_fc = nn.Linear(256,128) # outdims 128
        self.l13_fc = nn.Linear(128,4) # outdims 4
        
    def forward(self, x):
        x = self.relu(self.l1_conv(x))
        x = self.l2_pool(x)
        x = self.relu(self.l3_conv(x))
        x = self.l4_pool(x)
        x = self.dropout(x)
        x = self.relu(self.l5_conv(x))
        x = self.l6_pool(x)
        x = self.dropout(x)
        x = self.relu(self.l7_conv(x))
        x = self.l8_pool(x)
        x = self.dropout(x)
        x = self.relu(self.l9_conv(x))
        x = self.l10_pool(x)
        x = self.dropout(x)
        # Flatten
        x = x.view(-1,512)
        x = self.l11_fc(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.l12_fc(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.l13_fc(x)
        return x
