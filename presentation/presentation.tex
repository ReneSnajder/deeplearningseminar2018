\documentclass[12pt,usenames,dvipsnames]{beamer}
\usepackage{colortbl}
\usepackage[normalem]{ulem}
\usepackage{enumerate}
\usepackage{pgfpages}
\usepackage{graphicx}
\usepackage[sorting=none, backend=biber]{biblatex}
\usepackage{multirow}
\usepackage{array}


\geometry{paperwidth=140mm,paperheight=105mm}

\usetheme{Boadilla}
\usecolortheme{seahorse}

%\setbeameroption{show notes on second screen=right} % Both
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
\setbeamertemplate{footline}[frame number]
\beamertemplatenavigationsymbolsempty

\title{Classification of breast cancer histology images using Convolutional Neural Network \footfullcite{araujo}}
\subtitle{Seminar Biomedical Image Analysis: Deep Learning SS18}
\date{June 26, 2018}
\author{Rene Snajder}
\institute{Heidelberg University}

\newenvironment{changemargin}[2]{% 
  \begin{list}{}{% 
    \setlength{\topsep}{0pt}% 
    \setlength{\leftmargin}{#1}% 
    \setlength{\rightmargin}{#2}% 
    \setlength{\listparindent}{\parindent}% 
    \setlength{\itemindent}{\parindent}%
    \setlength{\parsep}{\parskip}% 
  }% 
  \item[]}{\end{list}} 

\begin{filecontents}{presentation.bib}
@techreport{araujo,
    title = {Classification of breast cancer histology images using Convolutional Neural Network},
    author = {Ara\'ujo, T. and Aresta, G. and Castro, E. and Rouco, J. and Aguiar, P. and Eloy, C. and Pol\'onia, A. and Campilho, A.},
    year = {2017},
    type = {PLoS One}
}

@article {wang2016deep,
	title = {Deep Learning for Identifying Metastatic Breast Cancer},
	journal = {arXiv preprint arXiv:1606.05718},
	year = {2016},
	author = {Wang, Dayong and Khosla, Aditya and Gargeya, Rishab and Irshad, Humayun and Andrew H Beck}
}

@INPROCEEDINGS{5193250, 
author={M. Macenko and M. Niethammer and J. S. Marron and D. Borland and J. T. Woosley and Xiaojun Guan and C. Schmitt and N. E. Thomas}, 
booktitle={2009 IEEE International Symposium on Biomedical Imaging: From Nano to Macro}, 
title={A method for normalizing histology slides for quantitative analysis}, 
year={2009}, 
month={June},}

\end{filecontents}
\addbibresource{presentation.bib}

\begin{document}

\begin{refsegment}
{
\setbeamertemplate{footline}{}
\maketitle 
}

\begin{frame}
\frametitle{Motivation}
\begin{block}{Problem}
\begin{itemize}
\item Breast cancer is one of the leading causes of death in women
\item Primary diagnostic method:
\begin{itemize}
\item biopsy of suspicious region
\item pathologist examins tissue under a microscope
\item diagnosis and staging of disease based on pathologist's findings
\end{itemize}
\item Manual examination is time consuming and biased
\end{itemize}
\end{block}
\begin{block}{Goal}
Automatic evaluation process using deep convolutional neural network. 
\end{block}


\end{frame}


\begin{frame}
\frametitle{Outline}
\large
\begin{itemize}
\item Introduction
\begin{itemize}
\item Tissue pathology in breast cancer
\item Image acquisition and H\&E staining
\item Problem definition
\item State-of-the-art
\end{itemize}
\item Material
\begin{itemize}
\item Dataset
\item Preprocessing
\end{itemize}
\item Methods
\begin{itemize}
\item Network architecture
\item Training
\end{itemize}
\item Results
\item Critical discussion
\begin{itemize}
\item Reproduction and comparison
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}

\section{Introduction}

\frametitle{Tissue pathology in breast cancer}

\begin{itemize}
\item Primary method of cancer diagnosis and staging
\item Process:
\begin{enumerate}
\item Suspicious region has been detected on mammogram
\item Biopsy: tissue sample taken from a suspicious region
\item Fixation of tissue using formalin to avoid decomposition
\item Embedding in a paraffin block
\item Cutting into 3-5{$\mu$}m thick slices and placement on glass slide
\item Slides are examined under a microscope
\end{enumerate}
\end{itemize}


\begin{center}
\includegraphics[width=0.9\textwidth]{images/tissueprocess}
\end{center}

\end{frame}



\begin{frame}
\frametitle{Image acquisition and H\&E staining}

\begin{block}{Microscopy}
\begin{itemize}
\item Transmitted light microscopy
\item Typically on multiple focus levels (recent development)
\item Output format: e.g. pyramidal tiled TIFF 
\end{itemize}
\end{block}

\begin{block}{H\&E Staining}
\begin{itemize}
\item Most cells are transparent
\item Dye binds to certain chemical structures to give them a bright color
\item \textbf{Haematoxylin} (basic)
\begin{itemize}
\item colors basicophilic structures purple/blue
\item mainly cell nuclei
\end{itemize}
\item \textbf{Eosin} (acidic)
\begin{itemize}
\item colors acidophilic structures pink
\item mainly cytoplasm and connective tissue
\end{itemize}
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Example image}

\only<1>{
  Multiple scales: 
  \begin{center}
  \includegraphics[height=0.9\textheight]{images/pyramid}
  \end{center}
}

\only<2>{
  Scale 3 (approx. 4{$\mu$}m per pixel): 
  \begin{center}
  \includegraphics[width=0.9\textwidth]{images/scale2}
  \end{center}
}

\only<3>{
  Scale 2 (approx. 1{$\mu$}m per pixel): 
  \begin{center}
  \includegraphics[width=0.9\textwidth]{images/scale1}
  \end{center}
}

\only<4>{
  Scale 1 (approx. 0.25{$\mu$}m per pixel): 
  \begin{center}
  \includegraphics[width=0.9\textwidth]{images/scale0}
  \end{center}
}

\only<5>{
Breast tissue is very heterogeneous!

  \begin{center}
  \includegraphics[width=0.75\textwidth]{images/examplelabeled}
  \end{center}

Not displayed: adipocytes (fat), plasma cells, blood vessels, ...

}
\end{frame}


\begin{frame}
\frametitle{Problem definition}

Two problems can be solved by machine learning:

\begin{enumerate}
\item Take whole image slide, and highlight regions with high tumor probability (guide diagnostician's attention)
\item Take a region selected by the diagnostician and classify (\textbf{this work})
\end{enumerate}


\begin{block}{Goal}
Given a \textcolor{red}{tissue slide image} and a preselected \textcolor{red}{region of arbitrary size} classify:

\begin{enumerate}
\item Normal tissue (non-carcinoma)
\item Benign lesion (non-carcinoma)
\item \emph{in situ} carcinoma
\item Invasive carcinoma
\end{enumerate}
\end{block}

\end{frame}

\begin{frame}
\frametitle{State-of-the-art}

Previous works typically deal with 2-class problems (benign/malignant or invasive/\emph{in situ}):

\begin{itemize}
\item Shallow-learning
\begin{itemize}
\item Image segmentation to find cell nuclei 
\item Manual feature construction (size, texture, shape)
\item Classification of cells (e.g. Random Forest)
\end{itemize}
\item Deep learning
\begin{itemize}
\item Image cut into patches of fixed size 
\item Patches classified using convolutional neural network
\item Make decision for entire image (e.g. threshold of positive patches)
\end{itemize}

\end{itemize}

\end{frame}


\begin{frame}
\frametitle{This work - Strategy}

\begin{itemize}
\item Cut image into patches
\item Use convolutional neural network to detect features from patches
\item Classify patches (softmax or SVM)
\item Classify image based on patch classification (voting)
\begin{itemize}
\item Majority voting
\item Maximum probability voting
\item Sum of probability voting
\end{itemize}
\end{itemize}

\end{frame}


\begin{frame}

\section{Material}

\frametitle{Dataset}

Dataset reused from 2015 breast histology classification challenge.

\begin{itemize}
\item $2048 \times 1536$ pixels
\item 0.42 {$\mu$}m per pixel
\item preselected regions of interest (not whole slide images)
\item training set: 249 images
\item test set: 20 images
\item extended test set: 36 images (16 additional ambiguous cases)
\end{itemize}

\begin{center}
\includegraphics[height=0.4\textheight]{images/benign_t6}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Dataset augmentation}

\begin{itemize}
\item full images are cut into patches of size $512 \times 512$ with 50\% overlap (35 patches per image)
\item horizontal flipping of patches
\item rotation of patches in 0, 90, 180, and 270 degrees
\item total in training set: $249 * 35 * 2 *4 = 69720$ patches
\end{itemize}

\begin{center}
\includegraphics[width=0.9\textwidth]{images/augmentation}
\end{center}


\end{frame}


\begin{frame}
\frametitle{Normalization 1/3}

\begin{block}{Problem}
Comparability of images suffers from batch effects
\begin{itemize}
\item differences in relative amount of stains used
\item differences between dye manufacturers
\item handling after the staining (bleaching)
\end{itemize}
\end{block}

Idea: Normalize all images to the same maximum H and E concentration \footfullcite{5193250}

Assumption: $OD = V*C$, where
\begin{itemize}
\item $I$ is pixel intensitites, $(3 \times n)$
\item $OD$ is optical density: $OD = - \log_{10}I$, $(3 \times n)$
\item $V$ is the stain matrix, $(3 \times 2)$, determines how a stain affects color
\item $C$ is the concentration of H and E at each point, $(2 \times n)$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Normalization 2/3}

Remember: $OD = V*C$

\begin{block}{Normalization process}
\begin{enumerate}
\item Compute $V$ by separating color space for H and E
\begin{enumerate}
\item Comptue eigenvalues of $OD$ using SVD
\item Take two eigenvectors with largest eigenvalues and span a plane
\item Project ODs onto that plane
\item For each point, calculate angle to first eigenvector 
\item Pick points with highest angle and lowest angle to be $V_1$ and $V_2$
\end{enumerate}
\item Compute $C$ by solving $C = V^{-1}*OD$
\end{enumerate}
\end{block}

\begin{center}
\includegraphics[width=0.30\textwidth]{images/stainseparation}
\small

Separation of color space
\end{center}

\end{frame}

\begin{frame}
\frametitle{Normalization 3/3}
Remember: $OD = V*C$
\begin{block}{Normalization process cont.}
\begin{enumerate}
\item Min/max normalize concentration to \textcolor{red}{target image}
\begin{enumerate}
\item Let $C_{Tmax}$ be the target maximum concentrations (predefined) and $C_{Smax}$ the
maximum source concentrations (maximum of our $C$)
\item $\hat C = C * C_{Tmax}/C_{Smax}$ (pointwise for both H and E)
\end{enumerate}
\item Compute normalized optical densities and finally intensities
\begin{enumerate}
\item $\hat{OD} = V * \hat C$
\item $\hat I = \exp(-\hat{OD})$
\end{enumerate}
\end{enumerate}
\end{block}

\begin{center}
\includegraphics[width=0.35\textwidth]{images/normcompare}
\end{center}

\end{frame}


\begin{frame}

\section{Methods}

\frametitle{Network Architecture}

\begin{center}
\includegraphics[width=0.9\textwidth]{images/cnn}
\vspace{0.2cm}

\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
 & \textbf{Type} & \textbf{Filter} & \textbf{Output} & \textbf{RF} & \textbf{RF} ({$\mu$}m) \\\hline
 & Input & & $3\times 512\times 512$ & $1\times 1$& $0.4 \times 0.4$\\\hline
Edge & Conv & $3\times 3$ & $16\times 510\times 510$ & $3\times 3$ & $1\times 1$\\
detection & Pool & $3\times 3$ & $16\times 170\times 170$ & $5\times 5$& $2\times 2$\\\hline
& Conv & $3\times 3$ & $32\times 168\times 168$ & $11\times 11$& $4.6\times 4.6$\\
Nuclei& Pool & $2\times 2$ & $32\times 84\times 84$ & $14\times 14$& $5.9\times 5.9$\\
detection & Conv & $3\times 3$ & $64\times 84\times 84$ & $26\times 26$& $11\times 11$\\
& Pool & $2\times 2$ & $64\times 42\times 42$ & $32\times 32$& $13\times 13$\\\hline
Structure& Conv & $3\times 3$ & $64\times 42\times 42$ & $56\times 56$& $24\times 24$\\
and  tissue& Pool & $3\times 3$ & $64\times 14\times 14$ & $80\times 80$& $34\times 34$\\
organization& Conv & $3\times 3$ & $32\times 12\times 12$ & $152\times 152$& $63.8\times 63.8$\\
& Pool & $3\times 3$ & $32\times 4\times 4$ & $224\times 224$& $94.1\times 94.1$\\\hline
& FC & 256 & & $512 \times 512$ & $215\times 215$\\\hline
& FC & 128 & & $512 \times 512$ & $215\times 215$\\\hline
& FC & 4 & & $512 \times 512$ & $215\times 215$\\\hline
\end{tabular}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Training}

\begin{itemize}
\item Patchwise training on $512 \times 512$ patches with $50 \%$ overlap
\item Cross validation:
\begin{itemize}
\item 75\% of training set for training
\item 25\% of training set for validation
\item randomly shuffled after each epoch
\item training stopped once validation error stabilized (after 50 epochs)
\end{itemize}
\item Regularization method: none documented  

Presumably Dropout layers after first two FC layers with $p=0.25$
\end{itemize}

\end{frame}


\begin{frame}

\section{Results}

\frametitle{Results - Patch-wise}

Accuracy:
\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
Classifier & No. Classes &Initial & Extended & Overall  \\\hline
& 4 & 72.5 & 59.4 & \textbf{66.7} \\ \cline{2-5} 
\multirow{-2}{*}{CNN} & 2 & 80.4 & 74.0 & \textbf{77.6} \\ \hline
 & 4 & 72.9 & 55.2 & 65.0 \\ \cline{2-5} 
\multirow{-2}{*}{CNN+SVM} & 2 & 82.9 & 69.3 & 76.9 \\ \hline
\end{tabular}
\end{center}


Sensitivity:
\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
 \multirow{2}{*}{Dataset} & \multirow{2}{*}{Classifier} & \multicolumn{2}{c|}{non-carcinoma} &\multicolumn{2}{c|}{carcinoma}  \\\cline{3-6}
 &  & Normal & Benign & \emph{in situ}  & Invasive\\\hline
\multirow{4}{*}{Initial} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{69.2} & \multicolumn{2}{c|}{91.7} \\ \cline{3-6} 
&& 61.7 & 56.7 & 83.3 & 88.3 \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{76.7} & \multicolumn{2}{c|}{89.2} \\ \cline{3-6} 
&& 65.0 & 61.7 & 76.7 & 88.3 \\ \hline

\multirow{4}{*}{Extended} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{81.3} & \multicolumn{2}{c|}{66.7} \\ \cline{3-6} 
&& 50 & 72.9 & 58.3 & 56.3 \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{82.3} & \multicolumn{2}{c|}{56.3} \\ \cline{3-6} 
&& 54.2 & 66.7 & 43.8 & 56.3 \\ \hline


\multirow{4}{*}{Overall} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{74.5} & \multicolumn{2}{c|}{\textbf{80.6}} \\ \cline{3-6} 
&& 56.4 & \textbf{63.9} & \textbf{72.2} & \textbf{74.1} \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{\textbf{79.2}} & \multicolumn{2}{c|}{74.5} \\ \cline{3-6} 
&& \textbf{60.2} & \textbf{63.9} & 62.0 & \textbf{74.1} \\ \hline


\end{tabular}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Results - Image-wise}

Accuracy:

\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|c|c|}\hline
  \multirow{2}{*}{Classifier} & \multirow{2}{*}{Voting method} & \multicolumn{3}{c|}{4 classes} & \multicolumn{3}{c|}{2 classes}  \\\cline{3-8}
& & Init. & Exten. & Overall &Init. & Exten. & Overall \\\hline
\multirow{3}{*}{CNN} & Majority & 80.0 & 75.0 & 77.8 & 80.0 & 81.3 & 80.6 \\\cline{2-8}
& Maximum prob. & 80.0 & 62.5 & 72.2 & 80.0 & 75.0 & 77.8 \\\cline{2-8}
& Sum of prob. & 80.0 & 68.8 & 75.0 & 80.0 & 75.0 & 77.8 \\\hline
\multirow{3}{*}{CNN+SVM} & Majority & 85.0 & 68.8 & 77.0 & 90.0 & 75.0 & \textbf{83.3} \\\cline{2-8}
& Maximum prob. & 80.0 & 62.5 & 72.2 & 80.0 & 75.0 & 77.8 \\\cline{2-8}
& Sum of prob. & 85.0 & 68.8 & \textbf{77.8} & 90.0 & 75.0 & \textbf{83.3} \\\hline
\end{tabular}
\end{center}

Sensitivity:
\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
 \multirow{2}{*}{Dataset} & \multirow{2}{*}{Classifier} & \multicolumn{2}{c|}{non-carcinoma} &\multicolumn{2}{c|}{carcinoma}  \\\cline{3-6}
 &  & Normal & Benign & \emph{in situ}  & Invasive\\\hline
\multirow{4}{*}{Initial} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{70} & \multicolumn{2}{c|}{90} \\ \cline{3-6} 
&& 80 & 40 & 100 & 100 \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{80} & \multicolumn{2}{c|}{100} \\ \cline{3-6} 
&& 80 & 60 & 100 & 100 \\ \hline

\multirow{4}{*}{Extended} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{50} & \multicolumn{2}{c|}{100} \\ \cline{3-6} 
&& 75 & 75 & 75 & 75 \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{50} & \multicolumn{2}{c|}{87.5} \\ \cline{3-6} 
&& 75 & 75 & 50 & 75 \\ \hline

\multirow{4}{*}{Overall} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{61.1} & \multicolumn{2}{c|}{94.4} \\ \cline{3-6} 
&& \textbf{77.8} & 55.6 & \textbf{88.9} & \textbf{88.9} \\ \cline{2-6}
& \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{\textbf{66.7}} & \multicolumn{2}{c|}{\textbf{94.4}} \\ \cline{3-6} 
&& \textbf{77.8} & \textbf{66.7} & 77.8 & \textbf{88.9} \\ \hline

\end{tabular}
\end{center}




\end{frame}

\begin{frame}
\frametitle{Results - Activations visualized}

Examples of activations for the first (A,B) and second (C) convolutional layers.

\begin{center}
\includegraphics[width=\textwidth]{images/featuresvis}
\end{center}

Different structures with diagnostic relevance are analyzed.

\end{frame}


\begin{frame}
\frametitle{Results - Feature visualization}

Features of second fully connected layer (dim. reduction with t-SNE)
\begin{center}
\includegraphics[width=\textwidth]{images/tsne}
\end{center}

\end{frame}


\begin{frame}

\section{Discussion}

\frametitle{Discussion - applicability to whole slide images}

\begin{itemize}
\item Not every region on a tissue slide is discriminative
\item Tumor may reside in only a part of the tissue  
\end{itemize}

\begin{center}
\includegraphics[width=0.7\textwidth]{images/probabilitymap}

\tiny (Image by Wang et al. \footfullcite{wang2016deep})
\end{center}

$\implies$ Voting process does not work here

$\implies$ Still requires a pathologist to preselect a region

\end{frame}


\begin{frame}
\frametitle{Discussion - Reproducability}

A number of reproducability issues arose:

\begin{itemize}
\item Code and model parameters uploaded by author were wrong
\item Several details were not documented in the publication:
\begin{itemize}
\item What border mode was used in convolution?
\item Was any form of regularization applied?
\item What were the target concentrations for normalization?
\end{itemize}
\item On 20.06.2018 authors sent me the correct model and test protocol
\begin{itemize}
\item Good news: Model worked, results match test protocol
\item Bad news: Results from test protocol do not match results documented in the paper
\end{itemize}

\end{itemize}


Sensitivities corrected:
\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
 \multirow{2}{*}{Dataset} & \multirow{2}{*}{Classifier} & \multicolumn{2}{c|}{non-carcinoma} &\multicolumn{2}{c|}{carcinoma}  \\\cline{3-6}
 &  & Normal & Benign & \emph{in situ}  & Invasive\\\hline
\multirow{2}{*}{Initial} & \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{80} & \multicolumn{2}{c|}{100} \\ \cline{3-6} 
&& 80 & 60 & 100 & 100 \\ \hline

\multirow{2}{*}{Extended} & \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{50 $\to$ \textcolor{OliveGreen}{100}} & \multicolumn{2}{c|}{90 $\to$ \textcolor{red}{62.5}} \\ \cline{3-6} 
&& 75 & 75 $\to$ \textcolor{OliveGreen}{100} & 50 $\to$ \textcolor{OliveGreen}{75} & 75 $\to$ \textcolor{red}{50} \\ \hline

\multirow{2}{*}{Overall} & \multirow{2}{*}{CNN+SVM} & \multicolumn{2}{c|}{66.7 $\to$ \textcolor{OliveGreen}{88.9} } & \multicolumn{2}{c|}{94.4 $\to$ \textcolor{red}{83.3}} \\ \cline{3-6} 
&& 77.8 & 66.7 $\to$ \textcolor{OliveGreen}{77.8} & 77.8 $\to$ \textcolor{OliveGreen}{88.9} & 88.9 $\to$ \textcolor{red}{77.8} \\ \hline

\end{tabular}
\end{center}



\end{frame}


\begin{frame}
\frametitle{Discussion - Comparison to state-of-the-art}

\small
\begin{tabular}{|c|c|c|c|c|}\hline
Method & Task & RF $\mu$m & Sens. & Acc. \\\hline
Cruz-Roa \emph{et al.} (2016)$^*$ & inv/non-inv & 25$\times$25 & 79.6\% & - \\\hline
Wang \emph{et al.} (2016) & classify metastasis & 70$\times$70 & 70.5\% & - \\\hline
Spanhol \emph{et al.} (2016) & malignant/benign & 18$\times$18 & - & 89.6\% \\\hline\hline

\multirow{2}{*}{\textbf{Ara\'ujo \emph{et al.} (2017)}} & inv/non-inv & 215$\times$215 & 74.1\% & 80\%  \\\cline{2-5}
 & malignant/benign & 215$\times$215 & 94.4$\% ^{**}$ & 83.3\% \\\hline
\end{tabular}

\vspace{1cm}

$^*)$ Training set contains labeled patches instead of whole-image labels

$^{**})$ Reported accuracy does not match test results, which suggest $62.5\%$ instead

\end{frame}



\begin{frame}
\frametitle{Reproduction attempt}

So, I attempted my own reproduction of the described architecture.

\vspace{0.5cm}

Technical details:
\begin{itemize}
\item Framework: pytorch
\item Zero-padding to match number of neurons
\item Softmax classifier (as SVM in paper showed only slightly better results)
\item Adam optimizer
\item Trained for about 20 epochs (validation error stabilized)
\end{itemize}

\vspace{0.5cm}

Initial results:
\begin{itemize}
\item Learns training set very well
\item High test error (image-wise accuracy $<$50\%)
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Reproduction attempt 2}

Needed to do something about overfitting:
\begin{itemize}
\item Didn't know how to get more data in the time available to me
\item Added Dropout regularization before each fully connected layer (prob. $0.5$)
\end{itemize}

\onslide<2>{
Results on test data:

\small
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|}\hline
\multirow{2}{*}{\textbf{Dataset}} & \multirow{2}{*}{\textbf{Label}} & \multicolumn{2}{c}{\textbf{Accuracy (\%)}} & \multicolumn{2}{|c|}{\textbf{Sensitivity(\%)}} & \multicolumn{2}{c|}{\textbf{Specificity (\%)}} \\\cline{3-8}
 & & Mine & Ara\'ujo & Mine & Ara\'ujo & Mine & Ara\'ujo \\\hline
\multirow{4}{*}{Initial} & Normal & 90 & 95 & 60 & 75 & 100 & 100 \\\cline{2-8}
&Benign & 80 & 85 & 80 & 60 & 80 & 75 \\\cline{2-8}
&\emph{in situ} & 90 & 95 & 80 & 100 & 93 & 83 \\\cline{2-8}
&Invasive & 100 & 95 & 100 & 100 & 100 & 83 \\\hline
\multirow{4}{*}{Overall} & Normal & 83 & 89 & 55 & 78  & 93 & 88 \\\cline{2-8}
& Benign & 75 & 86 & 78 & 78  & 75 & 70 \\\cline{2-8}
& \emph{in situ} & 89 & 94 & 67 & 88 & 96 & 89 \\\cline{2-8}
& Invasive & 92 & 92 & 78 & 78 & 96 & 88 \\\hline
\end{tabular}
\end{center}

Conclusion: Accuracies and sensitivities slightly lower than in the paper, but comparable

}
\end{frame}



\begin{frame}
\frametitle{Reproduction attempt 2 - Feature visualization}

Features of second fully connected layer (dim. reduction with t-SNE)
\begin{center}
\includegraphics[width=0.8\textwidth]{images/reprodtsne}
\end{center}
\end{frame}




\begin{frame}
\frametitle{Reprod. - Whole tissue slides from TCGA}

\begin{block}{TCGA - The Cancer Genome Atlas}
dataset comprising more than 11,000 samples on 33 types of cancer and different data types (genomics data, tissue slides, clinical info., etc)
\end{block}


Tested results on a few whole tissue slides and visualized patch classifications
\begin{itemize}
\item Slides in .svs format (3-4 scales, resolution up to 114,000 x 36,000px)
\item Used coursest scale to compute stain vectors for normalization
\item Extracted $215 \times 215${$\mu$}m patches (since images differ in resolution)
\item Normalized patches using precomputed stain vectors
\item Scaled patches to $512\times 512$ pixels
\item Computed prediction for all patches
\item Used threshold method to find patches containing tissue on the slide
\item Visualized prediction by overlaying colored patches
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{Reprod. - Whole tissue slides from TCGA}

\begin{center}
\includegraphics[width=0.6\textwidth]{images/examples}
\end{center}

Result:
\begin{itemize}
\item Detection of invasive tumor appears to work well
\item Loose connective tissue and fat are confused with \emph{in situ} carcinoma!
\item Problem: Training set did not contain images of such tissue regions!
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Conclusion}

\begin{block}{Pros}
\begin{itemize}
\item Deep Learning approach with filter sizes motivated by physics of the problem
\item Shows promising results given very limited training data
\item Multi-class classification combines diagnosis and staging
\end{itemize}
\end{block}

\begin{block}{Cons}
\begin{itemize}
\item Applicability to whole tissue slides requires further investigation
\item Ambiguities and errors  made reproduction a chore
\item Strongly dependent on region selection in tissue; does not work well on loose tissue
\item Lacks novelty (aside from the higher RF, it's very vanilla)
\item No ablation study: Does higher RF (for evaluation of tissue organization) actually improve results?
\end{itemize}
\end{block}


\end{frame}

\end{refsegment}


\begin{refsegment}
{
\setbeamertemplate{footline}{}
\maketitle 
}

\begin{frame}
\frametitle{Discussion - Reproducability (update)}

A last-minute update to the reproducability issue:

\begin{itemize}
\item On 25.06.2018 authors replied again with the following information:
\begin{itemize}
\item the script they sent me was still not the final one and was missing the priorization for tie-breaking
\item the SVM model was probably lost and the script contained the softmax version
\item if we modify it to do correct tie-breaking, we should get the correct sensitivities
\end{itemize}
\item I tried to do so, results compared to the reported results using softmax:
\end{itemize}


Sensitivities corrected:
\begin{center}
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|}\hline
 \multirow{2}{*}{Dataset} & \multirow{2}{*}{Classifier} & \multicolumn{2}{c|}{non-carcinoma} &\multicolumn{2}{c|}{carcinoma}  \\\cline{3-6}
 &  & Normal & Benign & \emph{in situ}  & Invasive\\\hline
\multirow{2}{*}{Initial} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{70 $\to$ \textcolor{red}{60}} & \multicolumn{2}{c|}{90 $\to$ \textcolor{OliveGreen}{100}} \\ \cline{3-6} 
&& 80 & 40 & 100 & 100 \\ \hline

\multirow{2}{*}{Extended} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{50 $\to$ \textcolor{OliveGreen}{87.5}} & \multicolumn{2}{c|}{100 $\to$ \textcolor{red}{75}} \\ \cline{3-6} 
&& 75 & 75 & 75 & 75 \\ \hline

\multirow{2}{*}{Overall} & \multirow{2}{*}{CNN} & \multicolumn{2}{c|}{61.6 $\to$ \textcolor{OliveGreen}{72.2} } & \multicolumn{2}{c|}{94.4 $\to$ \textcolor{red}{88.9}} \\ \cline{3-6} 
&& 77.8 & 55.6 & 88.9 & 88.9 \\ \hline

\end{tabular}
\end{center}

Remaining discrepencies can be explained by additional patch-wise priorization of severe classes (fuzzy tie-breaking) which is not documented.

\end{frame}

\end{refsegment}




\end{document}
